## 1. [1 pto] Indica la diferencia entre el uso de la instrucción CMD y ENTRYPOINT (Dockerfile).
<ins>ENTRYPOINT:</ins> Especifica un comando que se va a ejecutar siempre que se inicie un contenedor.

<ins>CMD:</ins> Especifica argumentos que se le pasarán al entrypoint del contenedor.

Cuando ejecutamos un contenedor Docker, este tiene por defecto su entrypoint seteado a la shell de su sistema (ej. /bin/sh, /bin/bash, etc).
La instrucción CMD nos permite pasarle un parámetro a éste entrypoint. Es decir, mediante el comando CMD podemos definir comandos a ejecutar mediante la shell del contenedor.
Por otro lado, ENTRYPOINT cambia el entrypoint por defecto del contenedor por los parámetros que le pasemos. Por lo tanto, al iniciar el contenedor se va a ejecutar dichos parámetros por defecto.

ENTRYPOINT se usa para crear containers ejecutables. Por el hecho que cambia el entrypoint, todos los parámetros que le pasemos al hacer run del contenedor, se van a ejecutar concatenados al entrypoint que hemos definido.
De este modo nos aseguramos que en el inicio del contenedor va a ejecutar por defecto nuestra aplicación.

Por otro lado, CMD sobreescribe la instrucción definida en el Dockerfile en caso que pasemos parámetros al hacer run del contenedor. Por este hecho puede ser considerado vulnerable usarlo para dockerizar un contenedor ejecutable, puesto que sobreescribirlo permitiria ejecutar cualquier comando dentro del contenedor.
#### Ejemplo:
```
FROM nginx:alpine    
ENTRYPOINT ["echo", "Hello"]  
CMD ["Docker"]
```
( Construimos la imagen...)
```
$ sudo docker run <image-id>  
Hello Docker  
$ sudo docker run <image-id> Adrià  
Hello Adrià
```

