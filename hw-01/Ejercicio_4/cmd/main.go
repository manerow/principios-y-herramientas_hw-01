package main

import "gitlab.com/manerow/principios-y-herramientas_hw-01/api"

const (
	PORT = "8080"
)

func main() {
	api := &api.Api{}
	api.InitialiseRoutes()
	api.Run(PORT)
}
