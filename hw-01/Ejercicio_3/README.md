## 3. [2,5 ptos] Crea un contenedor con las siguientes especificaciones: 
- Utilizar la imagen base NGINX haciendo uso de la versión 1.19.3.
 - Al acceder a la URL localhost:8080/index.html aparecerá el mensaje HOMEWORK 1.
 - Persistir el fichero index.html en un volumen llamado static_content.

Con el objetivo de crear un contenedor con las especificaciones establecidas, empezamos definiendo el Dockerfile de la imagen que queremos construir y el código html que nos permita mostrar el mensaje “HOMEWORK 1”.

#### Dockerfile
```
FROM nginx:1.19.3-alpine
EXPOSE 80
ADD index.html /usr/share/nginx/html/
```

Usamos como base la imagen nginx:1.19.3-alpine, versión ligera de la imagen establecida en las especificaciones del ejercicio.
Exponemos el puerto 80 del contenedor para poder acceder al servidor nginx desde el exterior.
Copiamos el fichero index.html creado previamente en el directorio raíz donde el servidor de nginx va a buscar los assets mediante el comando ADD.

#### index.html
```
<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <h1>
      HOMEWORK 1
    </h1>
  </body>
</html> 
```

#### Docker build
A continuación construimos la imagen Docker a partir de su definición establecida en el Dockerfile, para facilitar su identificación le asignamos el tag “nginx-ex-1”.
```
$ docker build . -t nginx-ex-1
```
#### Docker run
Finalmente ejecutamos la imagen y establecemos el volumen requerido, el puerto que se va a mapear de la máquina host al 80 del contenedor y el nombre del contenedor.
```
$ docker run -it -p 8080:80 -v static_content:/usr/share/nginx/html/ nginx-ex-1
```
Comprobamos que se ha creado el volumen: <br/>
![Volumen static_content](.bin/img/ex-3_1.png)

Comprobamos los detalles del contenedor en ejecución: <br/>
![Volumen static_content](.bin/img/ex-3_2.png)

Comprobamos que al acceder a localhost:8080 se muestra el mensaje “HOMEWORK 1”: <br/>
![Volumen static_content](.bin/img/ex-3_3.png)

